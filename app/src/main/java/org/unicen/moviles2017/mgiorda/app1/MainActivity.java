package org.unicen.moviles2017.mgiorda.app1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {

            private int counter = 0;

            @Override
            public void onClick(View v) {

                TextView text = (TextView) findViewById(R.id.textDisplay);

                counter++;

                text.setText("Button pressed! " + counter);
            }
        });
    }
}
